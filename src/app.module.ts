import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TransactionsModule } from './transactions/transactions.module';
import { PaymentModule } from './payment/payment.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Transaction } from './transactions/entities/transactions.entity';
import { HeadersService } from './headers/headers.service';
import {Payment } from './payment/entities/payment.entity';



@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true,}),
  TransactionsModule,
  PaymentModule,
  TypeOrmModule.forRoot({
    type: 'mysql',
    host: process.env.HOST,
    port: parseInt(<string>process.env.PORT, 10) || 3306,
    username: process.env.USER_NAME,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    entities: [Transaction,Payment],
    synchronize: true,
  })
],
  controllers: [],
  providers: [HeadersService],
})
export class AppModule {}

