import { Controller, Post, Body, Param, Get} from '@nestjs/common';
import { PaymentService } from './payment.service';
import { CreatePaymentDto } from './dto/create-payment.dto';


@Controller('api/v1')
export class PaymentController {
  constructor(private readonly paymentService: PaymentService) {}

  @Post('makepayment/:transaction_id')
   makePayment(@Body() body:CreatePaymentDto,@Param('transaction_id') transaction_id:string){
    return this.paymentService.makePayment(body,transaction_id)
  }
  
  @Get('paymentstatus/:transaction_id')
  getTransactionStatus(@Param('transaction_id') transaction_id:string){
    return this.paymentService.getTransactionStatus(transaction_id)
  }
}
