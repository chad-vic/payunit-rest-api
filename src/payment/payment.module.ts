import { Module } from '@nestjs/common';
import { PaymentService } from './payment.service';
import { PaymentController } from './payment.controller';
import { HeadersService } from 'src/headers/headers.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import {Transaction } from '../transactions/entities/transactions.entity';
import { TransactionsModule } from 'src/transactions/transactions.module';
import { Payment } from './entities/payment.entity';

@Module({
  controllers: [PaymentController],
  providers: [PaymentService,HeadersService],
  exports:[TypeOrmModule],
  imports: [TransactionsModule,TypeOrmModule.forFeature([Transaction,Payment])],
})
export class PaymentModule {}
