import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import axios from 'axios';
import { HeadersService } from 'src/headers/headers.service';
import { Repository } from 'typeorm';
import {Transaction } from '../transactions/entities/transactions.entity';
import { Payment } from './entities/payment.entity';


@Injectable()
export class PaymentService {
 constructor(@InjectRepository(Transaction) private transactionRepository: Repository<Transaction>,
 @InjectRepository(Payment) private paymentRepository: Repository<Payment>,
 private config: HeadersService){}

// make payment
 async makePayment(body,transaction_id){
    const {phone_number,gateway} = body

    if(gateway !== "mtnmomo" && gateway !== "orange"){
      throw new HttpException({
          status: HttpStatus.BAD_REQUEST,
          error: `Please gateway of type ${gateway} is not supported either use mtnmomo or orange`,
        }, HttpStatus.BAD_REQUEST)
  }

  if(typeof phone_number === "number"){
      phone_number.toString()
  }

  const isCameroonTel = /^6[5789]{1}[0-9]{7}$/.test(phone_number)

   if(!isCameroonTel) {
    throw new HttpException({
      status: HttpStatus.BAD_REQUEST,
      error: `Please ${phone_number} is not a valid orange or mtn phone number.`,
    }, HttpStatus.BAD_REQUEST)
   }

   const isTransaction = await this.transactionRepository.findOne({where:{transaction_id}}) 

   if(!isTransaction){
    throw new HttpException({
        status: HttpStatus.BAD_REQUEST,
        error: `No initiated transaction has transaction_id of ${transaction_id}`,
      }, HttpStatus.BAD_REQUEST)
}

   const {currency,total_amount:amount} =  isTransaction

   const paymentType = "button"

   const paymentData = {
     ...body,
     paymentType,
     currency,
     amount,
     transaction_id
   }


   try {
    const {data} = await axios.post(`${this.config.baseUrl()}/gateway/makepayment`,paymentData,this.config.setHearders())

    const {pay_token,payment_ref,auth_token,x_token,paytoken}  = data?.data

    const dbData = this.paymentRepository.create({pay_token,payment_ref,gateway,transaction_id,auth_token,x_token,paytoken})
    await this.paymentRepository.save(dbData)
     
    return data

   } catch (error) {
    return error?.response?.data
   }
}

// get transaction status
 async getTransactionStatus(transaction_id) {

  const isTransaction = await this.paymentRepository.findOne({where:{transaction_id}}) 

  if(!isTransaction){
   throw new HttpException({
       status: HttpStatus.BAD_REQUEST,
       error: `No payment has transaction_id of ${transaction_id}`,
     }, HttpStatus.BAD_REQUEST)
}

 const {pay_token,payment_ref,gateway,auth_token,x_token,paytoken} = isTransaction 

 if(gateway === "mtnmomo"){

  try {
      const {data} = await axios.get(`${this.config.baseUrl()}/gateway/paymentstatus/${gateway}/${transaction_id}?pay_token="${pay_token}"&payment_ref="${payment_ref}"`,this.config.setHearders())
      
      return data
    } catch (error) {

      return error?.response?.data
    }

 }  else if(gateway === "orange"){

  try {
    const {data} = await axios.get(`${this.config.baseUrl()}/gateway/paymentstatus/${gateway}/${transaction_id}?paytoken="${paytoken}"&auth-token="${auth_token}"&x-token="${x_token}"`,this.config.setHearders())  
    return data
    
  } catch (error) {
    return error?.response?.data
  }
 }

}

}



