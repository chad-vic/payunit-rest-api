import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { CreateTransactionsDto } from './dto/create-transaction.dto';
import { TransactionsService } from './transactions.service';

@Controller('api/v1/transaction')
export class TransactionsController {
  constructor(private readonly transactionsService: TransactionsService) {}
  
  @Post('initiate')
  initiatePayment(@Body() body:CreateTransactionsDto) {
    return this.transactionsService.initiatePayment(body)
  }

  @Get('getpsp/:transaction_id')
  getPsp(@Param('transaction_id') transaction_id:string) {
    return this.transactionsService.getPsp(transaction_id)
  }

}
