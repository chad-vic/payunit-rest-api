import { Module } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { TransactionsController } from './transactions.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Transaction } from './entities/transactions.entity';
import { HeadersService } from 'src/headers/headers.service';

@Module({
  controllers: [TransactionsController],
  providers: [TransactionsService,HeadersService],
  exports:[TypeOrmModule],
  imports: [TypeOrmModule.forFeature([Transaction])],
})
export class TransactionsModule {}
