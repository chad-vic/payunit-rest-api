import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Transaction } from './entities/transactions.entity';
import { nanoid } from 'nanoid/async';
import axios from 'axios';
import { HeadersService } from 'src/headers/headers.service';

@Injectable()
export class TransactionsService {
    constructor(@InjectRepository(Transaction) private transactionRepository: Repository<Transaction>,private config: HeadersService){}

  async initiatePayment(body){
        const {total_amount, currency,return_url} = body;

        if(currency !== "XAF" && currency !== "USD"){
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: `Please currency of type ${currency} is not valid either use XAF or USD`,
              }, HttpStatus.BAD_REQUEST)

        }

        // Generates unique id
        const transaction_id:string = await nanoid(18) 

        const transactionProperties = {
            ...body,
            transaction_id
        }
     
        try {
            const {data} = await axios.post(`${this.config.baseUrl()}/gateway/initialize`,transactionProperties,this.config.setHearders())
            const payload = data.data
            const {t_id,t_sum,t_url} = payload

            const encryptedProperties = {
                t_id,
                t_sum,
                t_url
            }

            const dbData = this.transactionRepository.create({...transactionProperties,...encryptedProperties})
            await this.transactionRepository.save(dbData)

            return {
                msg:"Transaction successfully initiated",
                transaction_id
            }
        } catch (error) {
            return error?.response?.data     
        }
    }


   async getPsp(transaction_id){

        if(!transaction_id){
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: `Please provide transaction_id`,
              }, HttpStatus.BAD_REQUEST)
        }

        const isTransaction = await this.transactionRepository.findOne({where:{transaction_id}}) 

        if(!isTransaction){
            throw new HttpException({
                status: HttpStatus.BAD_REQUEST,
                error: `No initiated transaction has transaction_id of ${transaction_id}`,
              }, HttpStatus.BAD_REQUEST)
        }

        const {t_url,t_id,t_sum} = isTransaction
        
        try {
            const {data} = await axios.get(`${this.config.baseUrl()}/gateway/gateways?t_url="${t_url}"&t_id="${t_id}"&t_sum="${t_sum}"`,this.config.setHearders())
            return data
        } catch (error) {
            return error?.response?.data   
        }
    }
}
